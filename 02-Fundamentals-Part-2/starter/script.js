'use strict';

// let hasDriversLicense = false;
// const passTest = true;

// if (passTest) hasDriversLicense = true;

// if (hasDriversLicense) console.log(`I can drive :🚘`);

// const interface = `Audio`;

// function logger() {
//   console.log(`My name is Marcus`);  
// }

// logger();
// logger();
// logger();

function fruitProcessor(apples,oranges) {
  const juice = `Juice with ${apples} apples and ${oranges} oranges.`;
  return juice;
}

const applejuice = fruitProcessor(5, 0);
console.log(applejuice);

const appleOrangeJuice = fruitProcessor(2,4);
console.log(appleOrangeJuice);

const orangeJuice = fruitProcessor(0,5);
console.log(orangeJuice);

const noJuice = fruitProcessor(0,0);
console.log(noJuice);

const num = Number(`23`);